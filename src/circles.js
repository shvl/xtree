const sqr = (x) => x*x;
const sqrt = Math.sqrt, abs = Math.abs, floor = Math.floor;

const getCircleCenter = (xc, yc, r1, x, r2) => {
  let x2 = xc, x1 = x, sign=1;
  if (x>xc){
    x1 = xc;
    x2 = x;
    sign = -1;
  }
  return sign*sqrt(sqr(r1+r2) - sqr(x2-x1));
}

const getDistance = (xc, yc, x, y) => {
  return floor(sqrt(abs(sqr(x - xc)+sqr(y - yc))))+1;
}

class Circles{
    constructor({height, width, offset, step}){
        this.height = height;
        this.width = width;
        this.offset = offset;
        this.step = step;
        this.circles = [];
        const g = sqrt(sqr(height) + sqr(width/2));
        this.halfWidth = this.width / 2;
        this.sin = this.halfWidth / g;
        this.cos = sqrt( 1- sqr(this.sin));
    }

    getLineX (y, factor){
        return this.halfWidth / this.height * y * factor + this.halfWidth
    }

    getLineY (y, factor){
        return this.height * y * factor / this.halfWidth - this.height
    }

    addCircle(realRadius){
        const circles = this.circles;
        const r = realRadius + this.offset;
        let circle;

        let res = [];
        for (let x=this.step; x<this.width-this.step; x+=this.step){
            circles.forEach((circle)=>{
                let d = getCircleCenter(circle.x, circle.y, circle.r, x, r);
                let y = circle.y + d;
                if (d && this.validateWithCircles(x, y, r) && this.validateWithLines(x, y, r))
                    res.push({x, y, r: realRadius})          
            })

            this.circleCrossLine(x, r, 1, res);
            this.circleCrossLine(x, r, -1, res);
        }

        circle = res.reduce(({min, res}, circle)=>{
            if (circle.y < min)
            return {min: circle.y, res: circle};
            return {min, res};
        }, {min: this.height, res: null}).res;

        if (circle)
            this.circles.push(circle);

        return circle;
    }

    validateWithCircles(x,y,r){
        return !this.circles.some(circle => getDistance(circle.x, circle.y, x, y) < circle.r + r)
    }

    validateWithLines(x,y,r){
        return !(x + r > this.getLineX(y, 1) || x-r < this.getLineX(y, -1) || y + r > this.height)
    }

    circleCrossLine(x, realRadius, factor, res){
        const r = realRadius + this.offset;
        const xc = x + factor * this.cos*r;
        const yc = this.sin*r - factor * this.getLineY(x, 1);
        if (xc && yc && this.validateWithCircles(xc, yc, r) && this.validateWithLines(xc, yc, r))
            return res.push({x: xc, y: yc, r: realRadius});
    }
}

export default Circles;