import React, { Component } from 'react';
import Circles from './circles';
import './App.css';

const CANVAS_WIDTH = 600, CANVAS_HEIGHT = 800, CIRCLE_COLOR = "#FFCCCC", CIRCLES_SIZE = [
  {radius: 5, probability: 30}, 
  {radius: 7.5, probability: 40}, 
  {radius: 10, probability: 100}, 
  {radius: 15, probability: 100}, 
  {radius: 20, probability: 100}, 
  {radius: 30, probability: 70}, 
  {radius: 50, probability: 20},
];

class App extends Component {
  constructor(){
    super();
    this.circles = new Circles({
      height: CANVAS_HEIGHT,
      width: CANVAS_WIDTH,
      step: 5,
      offset: 1
    });
   
   for (let i=0; i<250; i++)
    this.circles.addCircle(this.getRadius());

    this.state = { 
      count: 1,
      circles: this.circles.circles,
      colors: this.circles.circles.map( c=> ({
          r: this.random255()-10,
          g: this.random255(),
          b: this.random255()-10,
          a: Math.random()/3+0.1
        })
      )
    };
  }

  componentDidMount() {
    const context = this.refs.canvas.getContext('2d');
    this.setState((state)=> Object.assign(state, {context: context}))
    
    requestAnimationFrame(() => this.update() );
    this.changeOrder();
  }

  render() {
    return (
      <div className="App">
        <canvas ref="canvas" width={CANVAS_WIDTH} height={CANVAS_HEIGHT} />
      </div>
    );
  }

  changeOrder(){
    let {colors} = this.state;
    colors = colors.slice(1).concat(colors[0]);
    
    this.circles.circles = [];
    for (let i=0; i<250; i++)
      this.circles.addCircle(this.getRadius());

    this.setState(state => Object.assign(state, {circles: this.circles.circles, colors}));

    requestAnimationFrame(() => {
      this.update()
      setTimeout(()=>this.changeOrder(), 100);
    });
  }

  onCountChange(event){
    this.setState({count: parseInt(event.target.value, 10) || 1});
  }

  update(){
    this.state.context.clearRect(0,0,CANVAS_WIDTH,CANVAS_HEIGHT);
//    this.drawTriangle()
    this.state.circles.forEach((({x,y,r},i) => this.drawCircle(x, y, r, this.state.colors[i]) ))
  }

  drawCircle(x,y,r,c){
    const ctx = this.state.context;
    ctx.strokeStyle = CIRCLE_COLOR;
    ctx.beginPath();
    ctx.arc(x,y,r,0,2*Math.PI);
    if (c)
      ctx.fillStyle = `rgba(${c.r},${c.g},${c.b},${c.a})`;
    ctx.fill();
  }

  drawTriangle(){
    const ctx = this.state.context;
    ctx.beginPath();
    ctx.moveTo(0, CANVAS_HEIGHT);
    ctx.lineTo(CANVAS_WIDTH / 2, 0);
    ctx.lineTo(CANVAS_WIDTH, CANVAS_HEIGHT);
    ctx.fillStyle = '#363';
    ctx.fill();
  }

  getRadius(){
    const total = CIRCLES_SIZE.reduce((s,i)=> s + i.probability,0);
    const rand = Math.floor(Math.random() * total);
    return CIRCLES_SIZE.reduce(({total, radius}, circle)=>{
      if (radius)
        return {total, radius};
      return total <= circle.probability?{total, radius: circle.radius}:{total: total - circle.probability}
    },{total: rand}).radius;
  }

  random255 (){
    return 100+Math.floor(Math.random()*155);
  }
}

export default App;
